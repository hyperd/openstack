# OpenStack PoC

A PoC to experiment with OpenStack AIO.

This deployment relies on **Terraform** to lift the necessary resources and **Ansible** to provision and configure the services.
The current code is meant to be a *PoC* for the cloud, explicitly **DigitalOcean**, and with minor edits to the Terraform code, it can run on any other provider.

## Run it

This setup assumes you have configured terraform and its backend strategy. [Checkout mine](https://medium.com/faun/terraform-remote-backend-demystified-cb4132b95057). I've left the folder [backend](./backend) to complete the setup if you want to adopt the same strategy.

### Full deployment in the cloud

Edit the file [env.bash.example](./env.bash.example), adjust it with your settings if needed, and rename it to `env.bash`.

**Make sure** that your ssh key is in place.

Edit the [variables.tf](./variables.tf) file and adjust it according to your preferences, and make sure your machine will meet the minimum hardware requirements:

**Absolute minimum server resources (currently used for gate checks):**

- 8 vCPU’s
- 50GB free disk space on the root partition
- 8GB RAM

**Recommended server resources:**

- 8 CPU Cores
- 80GB free disk space on the root partition, or 60GB+ on a blank secondary disk. Using a secondary disk requires the use of the bootstrap_host_data_disk_device parameter.
- 16GB RAM

The terraform stack will create the hardware resources for you, and ansible will pick it up, leveraging a dynamic inventory plugin.
Check the playbook variables before triggering the deployment, modify what you need.

### Playbook vars

```yml
# toggle the os patching
openstack_patch_os: true
# allows installation in environment not directly exposed to the internet
openstack_force_offline_installation: true

### modify the following vars only if you know what you're doing.
openstack_ansible_repo: https://opendev.org/openstack/openstack-ansible
openstack_deployment_workdir: /opt/openstack-ansible
openstack_ansible_archive: openstack-ansible.tar.gz
```

Finally, trigger the deployment as follows:

```bash
chmod +x ./terraform-deploy && ./terraform-deploy
```

The script will do the job for you.

### Ansible deployment

You can skip terraform lifting the infra if you have it already or prefer a different setup.
Change dir to the `ansible` folder, and create the inventory with your servers, check the playbook vars, run it as follows:

```bash
cd ./ansible
ansible-playbook -i "$(pwd)/your-inventory" ./site.yml -v
```

I do recommend to run it with at least the first level of verbosity `-v`.

## Pitfalls

This deployment will last ~ 90 minutes, depending on your resources.
The last two tasks, specifically:

```yml
    - name: bootstrap the environment
      command: "./{{ item }}"
      args:
        chdir: "{{ openstack_deployment_workdir }}/scripts"
      loop:
        - bootstrap-ansible.sh
        - bootstrap-aio.sh

    - name: porvision openstack
      command: "{{ item }}"
      args:
        chdir: "{{ openstack_deployment_workdir }}/playbooks"
      loop:
        - openstack-ansible setup-hosts.yml
        - openstack-ansible setup-infrastructure.yml
        - openstack-ansible setup-openstack.yml
```

It will take the largest part of the time, and you won't get any output until each item of the loops completes successfully.

**DO NOT INTERRUPT THE EXECUTION** unless errored.

## License

[GNU General Public License v3 (GPLv3)](https://gitlab.com/hyperd/venvctl/blob/master/LICENSE)

## Report a Vulnerability

If you believe you have found a security flaw in this deployment, drop me a line to coordinate the vulnerability response and disclosure.

## About the author

[Francesco Cosentino](https://www.linkedin.com/in/francesco-cosentino/)

I'm a surfer, a crypto trader, and a DevSecOps Engineer with 15 years of experience designing highly-available distributed production environments and developing cloud-native apps in public and private clouds.

## References

**[terraform-inventory](https://github.com/adammck/terraform-inventory)** |
**[terraform-provider-ansible](https://github.com/nbering/terraform-provider-ansible/)** |
**[openstack-ansible](https://docs.openstack.org/openstack-ansible/latest/user/aio/quickstart.html)**
