terraform {  
    backend "s3" {
        bucket = "terraform-openstack-backend"
        key    = "terraform.tfstate"    
        region = "eu-central-1"
        dynamodb_table = "terraform-openstack-state-lock-dynamo"
    }
}
