variable "instance_count" {
  description = "Number of droplets to provision"
  default     = 1
  type        = number
}

variable "do_token" {
  description = "DigitalOcean API personal access token"
  type        = string
}

variable "do_region" {
  description = "DigitalOcean region"
  default     = "fra1"
  type        = string
}

variable "do_droplet_size" {
  description = "Default DigitalOcean droplet size"
  # default     = "s-16vcpu-64gb"
  # default     = "s-2vcpu-4gb"
  default     = "g-16vcpu-64gb"
  type        = string
}

variable "do_droplet_image" {
  description = "Default DigitalOcean droplet image"
  default     = "centos-8-x64"
  type        = string
}

variable "do_ssh_key" {
  description = "DigitalOcean ssh key name"
  default     = "APM3LC02ZR76SMD6T"
  type        = string
}

variable "private_key" {
  description = "Local path to the DigitalOcean ssh private key"
  type        = string
}
