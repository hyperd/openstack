data "digitalocean_ssh_key" "ssh_key" {
  name = var.do_ssh_key
}

resource "digitalocean_droplet" "openstack" {
  count               = var.instance_count
  image               = var.do_droplet_image
  name                = data.external.droplet_name.result.name
  region              = var.do_region
  size                = var.do_droplet_size
  backups             = false
  private_networking  = true
  ipv6                = true
  monitoring          = true

  ssh_keys = [
    data.digitalocean_ssh_key.ssh_key.id
  ]

  connection {
    host        = self.ipv4_address
    user        = "root"
    type        = "ssh"
    private_key = file(var.private_key)
    timeout     = "2m"
  }
}

# resource "local_file" "ansible_inventory" {
#   content     = "${join("\n", digitalocean_droplet.openstack.*.ipv4_address)}"
#   filename = "${path.module}/ansible/inventory"
# }

# resource "null_resource" "provision_openstack" {
#   triggers = {
#     droplet_ids = "${join(",", digitalocean_droplet.openstack.*.id)}"
#   }

#   provisioner "local-exec" {
#     command = "ansible-playbook -i ${path.module}/ansible/inventory ${path.module}/ansible/site.yml"
#   }
# }
